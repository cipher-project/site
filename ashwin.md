# Extras: Resources for Git
<a id="org20c8f66"></a>

## Using Git

This project is managed using git. The primary location of git host is:
<https://codeberg.org/cipher-project/cipher/>

1.  setting up the worktree

    setting up the worktree for git is easy. just type these commands in
    either a terminal emulator  or cmd.exe
    
        git config --global user.name "Name Here"
        git config --global user.email "your email"
        git clone 'https://codeberg.org/cipher-project/cipher/'
        cd c-project
        git checkout -b mybranch 
    
    These will setup the name, email and then prepare a new branch for you to work on.
    Please note that working in a new branch is highly recommended instead of using the master branch directly.

2.  Commiting

    If you did the above steps, you're on a new branch called `mybranch`.
       Do note that you should commit in a sense of completion of small tasks. this makes it easy to revert to certain period of history.
       After working, commit your changes with:
    
        git add "file you worked on" "file2" "file3" "etc"
        git commit 
    
    This will ask you for a commit message and a commit body. Put that as asked.

3.  Merging with upstream (Hasta la vista "pull request")

    After you've worked on your branch, it'd be nice if we could also look at it.
    To do that, simply prepare a patch and send an email containing the said patch.
    These commands create a patch which you can attach as a email attachment to
    ashwin (the at symbol) ashwink.com.np
    After sending the patch, I'll update that on the server but in your seperate branch.
    You can click on the branch link in project homepage to see your branch and commits that you've sent.
    
    You can use this commands to make .patch files which are to be attached in the email body.
    
        git format-patch origin
    
    Do note that git send-email is a lot better but requires some teeny tiny configuration.
    
    1.  Unrelated note (skip to next part)
    
        The reason for using email is because git was *made* to be used with email.
        Please note that terrible clients like \(gmail\) are terrible and for better email experience,
        you should think about using something like Thunderbird.
        For someone informed opinion: <https://drewdevault.com/2018/07/02/Email-driven-git.html>


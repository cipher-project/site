<a id="org05bcb99"></a>

# About

<iframe width="560" height="315" src="https://www.youtube.com/embed/nIxrWIEMR4U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<a id="orgd6a6f2f"></a>

Cipher is a very basic text encryption program.

It has two high level function.

-   TO ENCRYPT
    
    INPUT         &#x2013;&#x2014;> Processing -&#x2014;> OUTPUT
    
    Text file(normal)  -&#x2014;> encryption -&#x2013;&#x2014;> text file (encrypted)

-   TO DECRYPT
    
    INPUT         &#x2013;&#x2014;> Processing -&#x2014;> OUTPUT
    
    Text file(encrypted)  -&#x2014;> decryption -&#x2013;&#x2014;> text file (normal)
    
    TLDR: We open text file do sth to each alphabet and get unreadable file (but here the catch we can undo this process!)
	
<br></br>
# How user will use it

It is command line program so a terminal is needed.

-   By giving our program a input file

		$ cipher input.txt

-   Now cipher will generate output.txt in the same folder

-   INTERACTIVE MODE!
    
        $ cipher --interactive
    
    Will launch a prompt 
    
         Hello and welcome to cipher!
        
        >> 
    
    You can type words into the prompt and it will get encrypted! 
    
        Hello and welcome to cipher!
        
        >> hello
        eadfj
        
        >> raju
        kdsj

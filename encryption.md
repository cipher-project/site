<a id="org7c90d0a"></a>

# Resource: Encryption

The project is about creating a simple [Data Encryption Standard(DES)](https://en.wikipedia.org/wiki/Data_Encryption_Standard).
To be a little familiar with DES, you may want to learn about SDES from:

-   [G-SDES (Simpler DES )](http://mercury.webster.edu/aleshunas/COSC%205130/G-SDES.pdf)
-   [DES-Examples](http://ict.siit.tu.ac.th/~steven/css322y11s2/unprotected/CSS322Y11S2H01-DES-Examples.pdf)
-   [Example C implementation](https://toribeiro.com/simplified-DES-implementation-in-C/)

The Breaking down of project has not yet been done.
This is to be done after feedback from my fellow peers.

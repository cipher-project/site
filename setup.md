# Project Setup Guide

Following video will help you to 
- install C compiler and other necessary tools
- VsCode[OPTIONALLY], 

:::{note}
Watch only **FIRST 4 MINUTES** if you prefer other editor and **DO NOT WANT** to setup VsCode i.e it is not a requirement and any editor you are comfortable with will do just fine.
:::

<iframe width="560" height="315" src="https://www.youtube.com/embed/oaebkkOP2Qg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br></br>

# Tasks Guides

```{admonition} Guides for respective task
:class: warning
Every task will have its own detail guide to follow, if you want to do a task and its detail guide is unavailable you can ask it to be created.
```

## Task 6 : Detail Guide

- Download code zip from [this link](https://codeberg.org/cipher-project/cipher/archive/task6.zip)

## Check Lists
- Output the program in build/cipher.exe
- use C11 version 
- Show all warnings 
- Show warnings as errors
- Show extra warnings as well
- Disable redefining variables (declaring variables twice)
- Enable better debugging features

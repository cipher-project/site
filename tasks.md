<a id="orgf939900"></a>

# Tasks List

## 1. Writing welcome and help messages

   Program launch garda ke dekhincha r kasari use garne information.
   
   You will look at different command line programs to draw inspiration and techniques from and can be as creative as you can with the messages.
   

## 2. Validation logic and error messages

   Galat option r command diyo bhane error dini texts.

   You will look at various popular command line applications and learn about how they give error messages. How to guide user to use the program correctly and so on. 

:::{note}
After Task 1 and 2,

- You get familiarity of project codebase and how to contribute
- improve familiarity of command line application and how they operate
:::

  <br></br>

## 3. Reading text file

   Simple text file read garne function.

### C Concepts Used
   - fopen, fclose, fread
   - Loops


## 4. Writing out text file

   Simple text file write out garne function.

### C Concepts Used
   - fopen, fclose, fread, fwrite
   - Loops

  
:::{note}
You probably already have done 3 and 4. Choosing this task will mean you wil integrate that code into the project which includes polishing and editing the code to fit into the uniformity of rest of project. You may have to learn and switch to standard fwrite and fread function if you havent already.
:::

  <br></br>

## 5. Writing a logger setup

   It is similar to debugging using printf function. What we do is basically add printf statements in the parts of code to make sure every things is running correctly and write the state of program in a separate file. 

   You will have to write a funtion similar to printf that will write to a file called debug.log whenever we call this function.
   
   If anything goes wrong in our program, we can view this file and instantly know what our program was doing and where it failed. This is called logging and it helps in debugging our code.

### C Concepts Used
   - fopen, fclose, fread, fwrite
   - Loops
   - structs (optionally)

:::{note}
This task is extended version of read and writing files. If you want to strenthen your file handling skills this is a perfect fit.
:::

  <br></br>

## 6. Write Makefile 
```{admonition} Currently Assigned
:class: warning
Pradip
```

   Program compile and build logic.
   
   You will develop custom "compile and run" function availabel in code editors and know what really goes when building an exe file out of a C source file.
   
```{admonition} Why tho?
:class: info

For command line application like ours and in real world project management scenarios there are custom designed tools to build exe out fo source files. MakeFile will give us more power, customizability and understading. Eg. making compiler strict, enabling/disabling certain features, giving better warning and error message, switching C versions, customizing output exe name etc.   
```
   
  <br></br>

## 7. Parsing out the cli arguments to struct

   User le deko command and option yeuta struct ma organize garne. You will get list of string and classify them as commands and options.
   
   You will have to what options and commands the program offers the users with. Know how they are presented as a message to the users.
   
   
### C Concepts Used
   - Struct
   - string functions
   - functions and return values

### Extra Concepts
   - Basic knowledge of Command line programs
   - Basic knowledge of designing error handling and validation

  <br></br>

## 8. Core encryption logic

   Text r key file ko content lini r encrypted text nikalni function.

### C Concepts Used
   - Arrays
   - Binary operators
   - String functions
   - Extensive use of functions

### Extra Concepts
   - Researching and building out from theory
   - Cyber security and mathematical understadings

```{admonition} Currently Assigned
:class: warning
Ashwin

Choosing this tasks will mean you have to co-ordinate with Ashwin. You will learn about the implementation and theory first and then implement some of functions used in encrypting text files like permutation and xor operations.
```


<a id="org28a1158"></a>

<br></br>
# Sample Code Layout

    // ... c headers
    
    
    int main(//......){
    	 // Recieve input as commands from terminal
    	 struct Command command = parse_user_input()
    
    
    	 //if no commands Greet the user and give him instructions
    	 greet_with_about_and_help()
    
    	 // if User says a wrong command give hime usage errors
    	 validate_input_struct()
    
    	 // Write our own key array
    	 int *keys = {};
    
    	 // Read text file
    	 get_encrypted_text(file)
    
    	 // ENcrypt 
    	 get_encrypted_text(text, key)
    
    	 // write out the encrypted text
    	 get_encrypted_text(text, key)
    
    
    	 // if use says so launch interactive mode
    	 interactive_propmt_loop()
    }

